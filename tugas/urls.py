from django.urls import path
from . import views

app_name = 'main'

urlpatterns = [
    path('',views.home,name='index'),
    path('profile/',views.profile,name='story3'),
    path('workinpoggers/',views.workinpoggers,name='wip')
]